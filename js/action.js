function getData(){
    return new Promise((resolve) =>{
        var xhr = new XMLHttpRequest();// обращение к серверу
 xhr.open('GET','../data.json',true);
 xhr.onreadystatechange = function(){
    if(xhr.readyState==4){
        if(xhr.status==200){         
          resolve(JSON.parse(xhr.responseText));             
        }
    }
 }
 xhr.send();

    });
 

}
const DATA = getData(); 
//DATA.then(a => console.log(a));//data.then(function(result){console.log(result)};

/*function temporary(){
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(5) ;
        },3000);

    });
   
}
var res = temporary();
res.then(function(param){
    console.log(param);
});*/

const CATEGORIES = [
    {
        id:0,
        name:'Первые блюда'

    },
    {
        id:1,
        name:'Вторые блюда'
    },
    {
        id:2,
        name:'Десерты'
    },
    {
        id:3,
        name:'Напитки'
    }
];

var dishCounter=1;
var selectedDish;
var cart = [];

function generateFilterButtons(){
var filter = get('filter');//получаем filter из HTML
CATEGORIES.forEach(category => {
    var button = document.createElement('div');
    button.className = 'filter__button';
    button.innerText = category.name;
    button.addEventListener('click', () => {
        filterDishes(category.id);
    });
    filter.appendChild(button);

});
}
function filterDishes(id){
   var result =  allDishes.filter(item => item.category_id==id);
   generateDishes(result);
}


function generateDish(item){
    var content =get('content__menu');

    var dish =  `   <div class="dish">
    <img class="dish__img" src="./images/${item.image}" alt="">
    <div class="dish__info">
      <div class="dish__info-text">${item.name}<br>${item.description}</div> 
      <div class="dish__info-control">
          <span>Цена:${item.price}руб.</span>
          <div class="dish__info-control-button" onclick="buy(${item.id})">Купить</div>
      </div>
    </div>
</div>`;

content.innerHTML += dish;// в content menu отправили все содержимое тега dish
}

function buy(itemId){
     selectedDish = allDishes.find(item => item.id==itemId);
    displayOrder(selectedDish);
    getTotalSumm(selectedDish);
    openModal('modal'); 
    
}

function generateDishes(dishes) {
    var content = get('content__menu');
    content.innerHTML = '';
    dishes.forEach(dish =>{
        generateDish(dish);
    });

}

var get = function(name){
    return document.getElementsByClassName(name)[0];
}

var selectedCategoryId = CATEGORIES[0].id;//присваиваем id для переменной
var allDishes;

var modal;
var modalWindowClose;

/*function disapYelloWindow(){
    
    var modal = get('modal');
    modal.style.display = "none";
}
function closeYelloWindow(){
    var modalWindowClose = get('modal__window-close');
    modalWindowClose.addEventListener('click',disapYelloWindow);
}

function appYelloWindow(){
    var modal = get('modal');
    modal.style.display="";
}*/

function closeModal(elem){
    const modal = get(elem);
    modal.style.display = 'none';
    dishCounter=1;
}
function openModal(elem){
    if(elem=='cart-modal'){
        showCartItems();
    }
    const modal = get(elem);
    modal.style.display= 'flex';
    sendCounter(); 
}

function showCartItems(){

    const cart_area = get('cart-modal__window');
    cart_area.innerHTML = '';
    cart.forEach(elem =>{
        const item = ` <div class="cart-item">
        <div class="cart-item__info">
        <span class="cart-item__info-name " title="${elem.name}">${elem.name}</span>
        Цена:<span class="cart-item__info-price">${elem.price}</span>
        Кол-во: <span class="cart-item__info-count">${elem.count}</span>
        </div>
        <i class="far fa-times-circle cart-item__delete" onclick="deleteItem(${elem.id})"></i>

    </div>`;
    cart_area.innerHTML += item;
    });
}

function deleteItem(id){

    //cart.splice(0,1);
    console.log(cart);
   /* const dish = cart.find(elem => elem.id == id);//ищет элемент по id
    const index = cart.indexOf(dish); // ищет индекс в массиве
    cart.splice(index,1);//удаляет из массива
    console.log(`Корзина после удаления блюда`, cart);*/
    cart = cart.filter(elem => elem.id != id); // возврвщает массив эл-тов без объекта равного id
    showCartItems();

}

function help(e){
    e.stopPropagation();// останавливает закрытие при клике по модальному окну,ост.событие
}
function displayOrder(product){
    const item = get('modal__window-item');
    const card = `<div class="info">
    <img class="info__img" src="./images/${product.image}" alt="">
    <div class="info__description">${product.description}</div>
</div>

<div class="action">
    <div class="action__price">
        <span>Цена товара:<b> ${product.price}  руб.</b></span>
        <span >Общая стоимость:<b class="total-sum">1</b></span>
    </div>

    <div class="action__confirm">
        <div class="counter">
           <span class="counter__action" onclick="decrementCounter()">-</span> 
           <input class="counter__inp" type="text" onkeyup="checkInput(this,event)">
           <span class="counter__action" onclick="incrementCounter()">+</span>
        </div>

        <div class="confirm-button" onclick="confirmOrder()">Подтвердить заказ</div>
    </div>
</div>`;
item.innerHTML = card;

}

function getTotalSumm(dish){
    const summ = dish.price*dishCounter;
    const totalSumm = get('total-sum');
    totalSumm.innerText = Math.round(summ * 100) / 100;//округление до двух знаков после запятой

}

function checkInput(input,event){
    if(+event.key>=0&&event.key<10){ // + приводит строку к числу
        if(input.value>10){
            input.value=10;
        }
        return;
    }else{
        input.value=1;
    }
}
//var orderSum=0; 
function confirmOrder(){
    const counter = { // создали объект counter
        count: +get('counter__inp').value
    };
    Object.assign(selectedDish,counter); // слияние 2х объектов
    const isHaveItem = cart.some(elem => elem.id == selectedDish.id);
    if(isHaveItem){
        const item = cart.find(elem => elem.id == selectedDish.id);
        item.count += counter.count;
    }else{
        cart.push(selectedDish);
    }
    
    closeModal('modal');
    
    getCartSumm();
    //orderSum=orderSum+selectedDish.price;
    //console.log(orderSum);

}

function getCartSumm(){
    var summ = 0;
    cart.forEach(elem => {
        summ += elem.price*elem.count;// summ = summ + elem.price*elem.count
    });
    get('header__price').innerText = Math.round(summ*100)/100; //выводим стоимость в шапку
  /* allDishes.forEach((elem, index) =>{
    setTimeout(() => {
        console.log(`Элемент с индексом: ${index} равен: ${elem.name}`);

    }, 1000);  
   });*/
}



function incrementCounter(){
    if(dishCounter<10){ //if(dishCounter<10)dishCounter++;
    dishCounter++;
    }
    sendCounter();
    getTotalSumm(selectedDish);
    
}

function decrementCounter(){
    if(dishCounter>1){
    dishCounter--;
    }
    sendCounter();
    getTotalSumm(selectedDish);
}

function sendCounter(){
    const input = get('counter__inp');
    input.value = dishCounter;
}


window.onload = function(){
   // disapYelloWindow();
    generateFilterButtons();
   // closeYelloWindow();

    DATA.then(result =>{
       /* var dishes = result.filter(item => item.category_id ==selectedCategoryId);*/
       allDishes = result;
       
       var filteredDishes = allDishes.filter(item => item.category_id ==selectedCategoryId);
    
    generateDishes(filteredDishes);
      
    });
}